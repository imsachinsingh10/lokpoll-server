insert into db_logs (`dev`, scriptName) values ('RAVINDRA', '038_form_builder.sql');


DROP TABLE IF EXISTS `tbl_form`;

CREATE TABLE `tbl_form` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uid` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language` varchar(20) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ;


DROP TABLE IF EXISTS `tbl_form_input_fields`;

CREATE TABLE `tbl_form_input_fields` (
  `id` bigint(20) NOT NULL,
  `form_id` bigint(20) NOT NULL,
  `label` varchar(255) NOT NULL,
  `input_type` varchar(255) NOT NULL,
  `custom_value` varchar(255) DEFAULT NULL,
  `value_set` text DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `place_holder` varchar(255) DEFAULT NULL,
  `require` varchar(3) DEFAULT NULL,
  `show_input` varchar(4) DEFAULT NULL,
  `show_label` varchar(4) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ;

DROP TABLE IF EXISTS `tbl_form_input_values`;
CREATE TABLE `tbl_form_input_values` (
  `id` bigint(20) NOT NULL,
  `form_id` bigint(20) NOT NULL,
  `form_input_id` bigint(20) NOT NULL,
  `input_value` varchar(255) DEFAULT NULL
) ;


ALTER TABLE `tbl_form`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `tbl_form_input_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`);


ALTER TABLE `tbl_form_input_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `form_input_id` (`form_input_id`);



ALTER TABLE `tbl_form`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;


ALTER TABLE `tbl_form_input_fields`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;



ALTER TABLE `tbl_form_input_fields`
  ADD CONSTRAINT `tbl_form_input_fields_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `tbl_form` (`id`);


ALTER TABLE `tbl_form_input_values`
  ADD CONSTRAINT `tbl_form_input_values_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `tbl_form` (`id`),
  ADD CONSTRAINT `tbl_form_input_values_ibfk_2` FOREIGN KEY (`form_input_id`) REFERENCES `tbl_form_input_fields` (`id`);
