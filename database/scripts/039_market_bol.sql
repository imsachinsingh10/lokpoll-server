insert into db_logs (`dev`, scriptName) values ('Sachin', '038_form_builder.sql');


DROP TABLE IF EXISTS `market`;

CREATE TABLE `market` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
);

INSERT INTO `market` (`id`, `name`) VALUES
(1, 'Product'),
(2, 'Service'),
(3, 'Properties');


DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `marketId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` text NOT NULL,
  `deletedAt` datetime DEFAULT NULL
);

DROP TABLE IF EXISTS `template`;

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `templateName` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `marketId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) NOT NULL,
  `deletedAt` datetime DEFAULT NULL
);

DROP TABLE IF EXISTS `attribute`;
CREATE TABLE `attribute` (
  `id` int(11) NOT NULL,
  `attributeName` varchar(255) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `deletedAt` datetime DEFAULT NULL
);

DROP TABLE IF EXISTS `attribute_value`;
CREATE TABLE `attribute_value` (
  `id` int(11) NOT NULL,
  `attributeValueName` varchar(255) NOT NULL,
  `attributeId` int(11) NOT NULL
);

ALTER TABLE `gallery` ADD `thumbnailUrl` VARCHAR(255) NULL DEFAULT NULL AFTER `createdDate`;

ALTER TABLE `user` ADD `profileImage` VARCHAR(255) NULL DEFAULT NULL AFTER `coins`;

ALTER TABLE `market`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MARKET_CATEGORY` (`marketId`);

ALTER TABLE `template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MARKET_TEMPLATE` (`marketId`);
  
ALTER TABLE `attribute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ATTRIBUTE_CATEGORY` (`categoryId`);

ALTER TABLE `attribute_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ATTRIBUTE_VALUE_ATTRIBUTE` (`attributeId`);  
  
ALTER TABLE `attribute_value`
  ADD CONSTRAINT `ATTRIBUTE_VALUE_ATTRIBUTE` FOREIGN KEY (`attributeId`) REFERENCES `attribute` (`id`);  
  
ALTER TABLE `category`
  ADD CONSTRAINT `MARKET_CATEGORY` FOREIGN KEY (`marketId`) REFERENCES `market` (`id`);
  
ALTER TABLE `template`
  ADD CONSTRAINT `MARKET_TEMPLATE` FOREIGN KEY (`marketId`) REFERENCES `market` (`id`);
  
ALTER TABLE `attribute`
  ADD CONSTRAINT `ATTRIBUTE_CATEGORY` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`);  
        
ALTER TABLE `market`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;  

ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
ALTER TABLE `attribute_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;  
  
