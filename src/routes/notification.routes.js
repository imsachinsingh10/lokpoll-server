import express from 'express';
import {validateAuthToken} from "../middleware/auth.middleware";
import {NotificationService} from "../service/notification.service";
import AppOverrides from "../service/common/app.overrides";
import {log} from "../service/common/logger.service";
import {AppCode} from "../enum/app-code";
import {HttpCode} from "../enum/http-code";
import {MinIOService, uploadFile} from "../service/common/minio.service";

const router = express();
export class NotificationRoutes {
    constructor(app) {
        new AppOverrides(router);
        app.use('/notification', router);

        this.NotificationService = new NotificationService();
        this.minioService = new MinIOService();
        this.initRoutes();
    }

    initRoutes() {
        router.use(validateAuthToken);

        router.post('/add', async (req, res) => {
           
            try {
                let notificationData = {
                    title: req.body.title,
                    type: req.body.type,
                    status: req.body.status,
                    // createdDate: req.body.createdDate,
                
                }; 
                await this.NotificationService.addNotificationData(notificationData);
                return res.sendStatus(HttpCode.ok);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.s3_error) {
                    return res.status(HttpCode.bad_request).send(e);
                }
                return res.status(HttpCode.internal_server_error).send(e);
            }
        });

        router.post('/list', async (req, res) => {
            try {
                

                let notifications = await this.NotificationService.getNotificationData(req.body);
                // return res.json({'status':HttpCode.ok,'data':images});
                return await res.json(notifications);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.s3_error) {
                    return res.status(HttpCode.bad_request).send(e);
                }
                return res.status(HttpCode.internal_server_error).send(e);
            }
        });

        router.post('/update', async (req, res) => {
           
            try {
                let notificationData = {
                    id : req.body.id,
                    // title: req.body.title,
                    // type: req.body.type,
                    status: req.body.status,
                    // createdDate: req.body.createdDate,
                
                }; 
                await this.NotificationService.updateNotificationData(notificationData);
                return res.sendStatus(HttpCode.ok);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.s3_error) {
                    return res.status(HttpCode.bad_request).send(e);
                }
                return res.status(HttpCode.internal_server_error).send(e);
            }
        });

        //------------------------using get---------------------        


        router.get('/delete/:notificationId', async (req, res) => {
            try {
                // console.log(req.params.galleryId);
                // return false;
                await this.NotificationService.deleteNotificationData(req.params.notificationId);
                return res.sendStatus(HttpCode.ok);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                return res.sendStatus(HttpCode.internal_server_error);
            }
        }); 
    }
}