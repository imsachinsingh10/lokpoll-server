import express from 'express';
import {validateAuthToken} from "../middleware/auth.middleware";
import {AttributeService} from "../service/attribute.service";
import AppOverrides from "../service/common/app.overrides";
import {log} from "../service/common/logger.service";
import {AppCode} from "../enum/app-code";
import {HttpCode} from "../enum/http-code";
import {MinIOService, uploadFile} from "../service/common/minio.service";


const router = express();
export class AttributeRoutes {
    constructor(app) {
        new AppOverrides(router);
        app.use('/attribute', router);

        this.AttributeService = new AttributeService();
        this.minioService = new MinIOService();
        this.initRoutes();
    }

    initRoutes() {
        router.use(validateAuthToken);

        router.post('/add', async (req, res) => {
           
        //    console.log(req.file);
        //    return false;
            try {
                let attributeData = {
                    attributeName: req.body.attributeName,
                    // status: req.body.status,
                    categoryId: req.body.categoryId,
                
                }; 
                // if (req.file) {
                //     const file = await this.minioService.uploadFile(req.file);
                //     galleryData.fileName = file.url;
                // }
                await this.AttributeService.addAttributeData(attributeData);
                return res.sendStatus(HttpCode.ok);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.s3_error) {
                    return res.status(HttpCode.bad_request).send(e);
                }
                return res.status(HttpCode.internal_server_error).send(e);
            }
        });


        router.post('/list', async (req, res) => {
                try {
                    
                    let attributeList = [];
                    let attributes = await this.AttributeService.getAttributeData(req.body);
                    for (const attribute of attributes) {  
                        attribute.attributeValue = await this.AttributeService.getAttributeValueData(attribute.id);
                        attributeList.push(attribute);
                      }
                    return await res.json(attributeList);
                } catch (e) {
                    log.e(`${req.method}: ${req.url}`, e);
                    if (e.code === AppCode.s3_error) {
                        return res.status(HttpCode.bad_request).send(e);
                    }
                    return res.status(HttpCode.internal_server_error).send(e);
                }
            });

        router.post('/update', async (req, res) => {
            try {
                const attributeData = 
                {
                    id : req.body.id,
                    categoryId:req.body.categoryId,
                    attributeName : req.body.attributeName,
                    // language: req.body.language,
                    status : req.body.status
                };
                    await this.AttributeService.updateAttributeData(attributeData);
                    return res.sendStatus(HttpCode.ok);
                } 
            catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                return res.sendStatus(HttpCode.internal_server_error);
                    }
            });

            
// //------------------------using get---------------------        


            // router.get('/delete/:attributeId', async (req, res) => {
            //     try {
            //         // console.log(req.params.galleryId);
            //         // return false;
            //         deletedAt: 'utc_timestamp()';
            //         await this.AttributeService.deleteAttributeData(req.params.galleryId);
            //         return res.sendStatus(HttpCode.ok);
            //     } catch (e) {
            //         log.e(`${req.method}: ${req.url}`, e);
            //         return res.sendStatus(HttpCode.internal_server_error);
            //     }
            // });        


// //------------------using post--------------


            router.post('/delete', async (req, res) => {
                try {
                    const attributeData = {
                        id : req.body.id,
                        deletedAt : 'utc_timestamp()',
                    } ;
                    // console.log(attributeData);
                    // return false;
                    await this.AttributeService.deleteAttributeData(attributeData);
                    return res.sendStatus(HttpCode.ok);
                } catch (e) {
                    log.e(`${req.method}: ${req.url}`, e);
                    return res.sendStatus(HttpCode.internal_server_error);
                }
            });        

    }

}