import express from 'express';
import {HttpCode} from "../enum/http-code";
import {AppCode} from "../enum/app-code";
import {FormBuilderService} from "../service/FormBuilder.service"
import AppOverrides from "../service/common/app.overrides";
import {validateAuthToken} from "../middleware/auth.middleware";
import {log} from "../service/common/logger.service";
import * as _ from 'lodash';

const router = express();

export class FormBuilderRoutes {
    constructor(app) {
        new AppOverrides(router);
        app.use('/formbuilder', router);

        this.formBuilderService = new FormBuilderService();
        this.initRoutes();
    }

    initRoutes() {
       router.use(validateAuthToken);

        router.post('/add-form', async (req, res) => {
            try {
				
				let frmBldData = {
                    name: req.body.formName,
                    uid: req.body.uid,
					slug: await this.formBuilderService.convertToSlug(req.body.formName),
					//created_at: 'utc_timestamp()',                    
                }; 
				
				if (req.body.language)
				{
					if (!_.isEmpty(req.body.language.trim())) 
					{
						frmBldData.language=req.body.language.trim();
					}								
				}
				
				await this.formBuilderService.createForm(frmBldData);
                return res.sendStatus(HttpCode.ok);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.duplicate_entity) {
                    return res.status(HttpCode.bad_request).send(e);
                }
                return res.sendStatus(HttpCode.internal_server_error);
            }
        });
		
		router.post('/add-form-input', async (req, res) => {
            try {
								
				if ( req.body instanceof Array ) 
				{
					
					let data = req.body;
					 data.forEach(obj => {
                       let frmBldData = {
						   form_id: obj.form_id,
						   label: obj.label,
						   input_type: obj.input_type,
						   custom_value: obj.custom_value,
						   value_set: obj.value_set,
						   default_value: obj.default_value,
						   place_holder: obj.place_holder,
						   require: obj.require,
						   show_input: obj.show_input,
						   show_label: obj.show_label,
						    }; 
					   
                        this.formBuilderService.createInputFields(frmBldData);
                      });
                }
				else{
					
				let frmBldData = {
					form_id: req.body.form_id,
					label: req.body.label,
					input_type: req.body.input_type,
					custom_value: req.body.custom_value,
					value_set: req.body.value_set,
					default_value: req.body.default_value,
					place_holder: req.body.place_holder,
					require: req.body.require,
					show_input: req.body.show_input,
				    show_label: req.body.show_label,
					//created_at: 'utc_timestamp()',
                
                }; 
				
				
				await this.formBuilderService.createInputFields(frmBldData);
				
				}
				
                return res.sendStatus(HttpCode.ok);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.duplicate_entity) {
                    return res.status(HttpCode.bad_request).send(e);
                }
                return res.sendStatus(HttpCode.internal_server_error);
            }
        });

        router.post('/get-forms', async (req, res) => {
            try {
				let frmGetData = {
					slug: req.body.slug,
					id: req.body.form_id,
					
				};
				
                let formData = await this.formBuilderService.getForm(frmGetData);
				if (_.isEmpty(formData)) {
					
					//return res.status(HttpCode.bad_request).send(`Form not found!`);
					return res.json({
						status: HttpCode.ok,
						message: "No record found",
						data: formData
						});
				}
				let formInputFiled = [];
				let formDataInput = await this.formBuilderService.getFormInput(formData[0]);
				for (let inputfld of formDataInput){
					if(inputfld.valueSet!="" && inputfld.valueSet!=null){
						inputfld.valueSet = JSON.parse(inputfld.valueSet);
					}else{
						inputfld.valueSet = [];
					}
					formInputFiled.push(inputfld);
				}

                let ret = await this.formBuilderService.GetCustomValueData(formDataInput, formData);
				
				let jsonObj =formData[0];
				jsonObj.form_inputs=formDataInput;
				 
								
                return await res.json(jsonObj);
				
				
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.invalid_creds) {
                    return res.status(HttpCode.unauthorized).send(e);
                }
                res.sendStatus(HttpCode.internal_server_error);
            }
        });

        

       router.post('/update-form', async (req, res) => {
            try {
				
                let frmBldData = 
                {
					id: req.body.form_id,
                    name: req.body.formName,
					slug: await this.formBuilderService.convertToSlug(req.body.formName), 
                    uid: req.body.uid,
					//created_at: 'utc_timestamp()',
                };
				
				if (req.body.language)
				{
					if (!_.isEmpty(req.body.language.trim())) 
					{
						frmBldData.language=req.body.language.trim();
					}								
				}
                   
                    await this.formBuilderService.updateForm(frmBldData);
                    return res.sendStatus(HttpCode.ok);
                } 
            catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                return res.sendStatus(HttpCode.internal_server_error);
                    }
            });

        
		
		router.post('/update-form-input', async (req, res) => {
            try {
				
				if ( req.body instanceof Array ) 
				{
					
					let data = req.body;
					 data.forEach(obj => {
                       let frmBldData = {
						   id: obj.form_input_id,
						   form_id: obj.form_id,
						   label: obj.label,
						   input_type: obj.input_type,
						   custom_value: obj.custom_value,
						   value_set: obj.value_set,
						   default_value: obj.default_value,
						   place_holder: obj.place_holder,
						   require: obj.require,
						   show_input: obj.show_input,
						   show_label: obj.show_label,
						    }; 
					   
                        this.formBuilderService.updateInputFields(frmBldData);
                      });
                }
				else{
					let frmBldData = 
							{
							id: req.body.form_input_id,
							form_id: req.body.form_id,
							label: req.body.label,
							input_type: req.body.input_type,
							custom_value: req.body.custom_value,
							value_set: req.body.value_set,
							default_value: req.body.default_value,
							place_holder: req.body.place_holder,
							require: req.body.require,
							show_input: req.body.show_input,
						    show_label: req.body.show_label,
							//created_at: 'utc_timestamp()',
							};
                   
                    await this.formBuilderService.updateInputFields(frmBldData);
					
					}
                    return res.sendStatus(HttpCode.ok);
                } 
            catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                return res.sendStatus(HttpCode.internal_server_error);
                    }
            });
			
			router.post('/delete-form', async (req, res) => {
				try{
					
					let frmBldData = {
					id: req.body.form_id,
                    deleted_at: 'utc_timestamp()',
                };
				
					await this.formBuilderService.deleteForm(frmBldData);
                    return res.sendStatus(HttpCode.ok);
					
				}catch (e) {
					log.e(`${req.method}: ${req.url}`,e);
					return res.sendStatus(HttpCode.internal_server_error);
				}
			});
			
			router.post('/delete-form_input', async (req, res) => {
				try{
					
					let frmBldData = {
					id: req.body.form_input_id,	
					form_id: req.body.form_id,					
                    deleted_at: 'utc_timestamp()',
                };
					await this.formBuilderService.deleteFormInput(frmBldData);
                    return res.sendStatus(HttpCode.ok);
					
				}catch (e) {
					log.e(`${req.method}: ${req.url}`,e);
					return res.sendStatus(HttpCode.internal_server_error);
				}
			});
			
			
			router.post('/duplicate-form', async (req, res) => {
            try {
				
				let frmBldData = {
                    form_id: req.body.form_id,
                 }; 
											
				
				let DataM = await this.formBuilderService.getFormById(frmBldData);
		        
				if (_.isEmpty(DataM)) {
					//return res.status(HttpCode.bad_request).send(`Form not found!`);
						return res.json({
						status: HttpCode.ok,
						message: "No record found",
						data: DataM
						});
						
				}else{
					
					let Data ={
						name: DataM[0].name,
						uid: DataM[0].uid,
						slug: await this.formBuilderService.convertToSlug(DataM[0].name),
						language: DataM[0].language,
					};
	    			
					let insertedData = await this.formBuilderService.createForm(Data);
					
					let DataD = await this.formBuilderService.getFormInput(frmBldData);
				    
					if (!_.isEmpty(DataD)) {
					    
						let data=DataD;
						data.forEach(obj => {
							let frmBldInputData = {
								form_id: insertedData.insertId,
								label: obj.label,
								input_type: obj.type,
								custom_value: obj.customValueSet,
								value_set: obj.valueSet,
								default_value: obj.defaultValue,
								place_holder: obj.placeholder,
								require: obj.requiredValue,
								show_input: obj.showInput,
								show_label: obj.showLabel,
							}; 

                        this.formBuilderService.createInputFields(frmBldInputData);
                      });
					  
				       }
				}
				
				 return res.sendStatus(HttpCode.ok);
					
				}catch (e) {
					log.e(`${req.method}: ${req.url}`,e);
					return res.sendStatus(HttpCode.internal_server_error);
				}
        });
		
		
		router.post('/get-all-forms-list', async (req, res) => {
            try {
				
				let frmGetData = {
					limit: req.body.limit,
                    page: req.body.page,
					//uid: req.body.uid,
				};
				
				
				
                let formData = await this.formBuilderService.getAllFormsList(frmGetData);
				
				//console.log(formData);
				
				if (_.isEmpty(formData)) {
					
					//return res.status(HttpCode.bad_request).send(`Form not found!`);
					return res.json({
						status: HttpCode.ok,
						message: "No record found",
						data: formData
						});
				}
		
				
		        								
                return await res.json(formData);
				
				
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.invalid_creds) {
                    return res.status(HttpCode.unauthorized).send(e);
                }
                res.sendStatus(HttpCode.internal_server_error);
            }
        });

		router.post('/get-form_input_data', async (req, res) => {
			try{
				
				let frmBldData = {
				form_input_id: req.body.form_input_id,	
				form_id: req.body.form_id,					
			    };

				let inputData = await this.formBuilderService.getFormInputData(frmBldData);
				if (_.isEmpty(inputData)) {
					
					return res.json({
						status: HttpCode.ok,
						message: "No record found",
						value: inputData
						});
				}
		
				return await res.json(inputData);
				
			}catch (e) {
				log.e(`${req.method}: ${req.url}`,e);
				return res.sendStatus(HttpCode.internal_server_error);
			}
		});
                

    }
}
