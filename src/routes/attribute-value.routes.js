import express from 'express';
import {validateAuthToken} from "../middleware/auth.middleware";
import {AttributeValueService} from "../service/attribute-value.service";
import AppOverrides from "../service/common/app.overrides";
import {log} from "../service/common/logger.service";
import {AppCode} from "../enum/app-code";
import {HttpCode} from "../enum/http-code";
import {MinIOService, uploadFile} from "../service/common/minio.service";


const router = express();
export class AttributeValueRoutes {
    constructor(app) {
        new AppOverrides(router);
        app.use('/attributeValue', router);

        this.AttributeValueService = new AttributeValueService();
        this.minioService = new MinIOService();
        this.initRoutes();
    }

    initRoutes() {
        router.use(validateAuthToken);

        router.post('/add', async (req, res) => {
           
        //    console.log(req.file);
        //    return false;
            try {
                let attributeValueData = {
                    attributeValueName: req.body.attributeValueName,
                    // status: req.body.status,
                    attributeId: req.body.attributeId,
                
                }; 
                // if (req.file) {
                //     const file = await this.minioService.uploadFile(req.file);
                //     galleryData.fileName = file.url;
                // }
                await this.AttributeValueService.addAttributeValueData(attributeValueData);
                return res.sendStatus(HttpCode.ok);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.s3_error) {
                    return res.status(HttpCode.bad_request).send(e);
                }
                return res.status(HttpCode.internal_server_error).send(e);
            }
        });


    }

}