import express from 'express';
import {validateAuthToken} from "../middleware/auth.middleware";
import {TemplateService} from "../service/template.service";
import AppOverrides from "../service/common/app.overrides";
import {log} from "../service/common/logger.service";
import {AppCode} from "../enum/app-code";
import {HttpCode} from "../enum/http-code";
import {MinIOService, uploadFile} from "../service/common/minio.service";


const router = express();
export class TemplateRoutes {
    constructor(app) {
        new AppOverrides(router);
        app.use('/template', router);

        this.TemplateService = new TemplateService();
        this.minioService = new MinIOService();
        this.initRoutes();
    }

    initRoutes() {
        router.use(validateAuthToken);

        router.post('/add', async (req, res) => {
           
        //    console.log(req.file);
        //    return false;
            try {
                let templateData = {
                    templateName: req.body.templateName,
                    language: req.body.language,
                    status: req.body.status,
                    marketId: req.body.marketId,
                
                }; 
                // if (req.file) {
                //     const file = await this.minioService.uploadFile(req.file);
                //     galleryData.fileName = file.url;
                // }
                await this.TemplateService.addTemplateData(templateData);
                return res.sendStatus(HttpCode.ok);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.s3_error) {
                    return res.status(HttpCode.bad_request).send(e);
                }
                return res.status(HttpCode.internal_server_error).send(e);
            }
        });


        router.post('/list', async (req, res) => {
                try {
                    

                    let categories = await this.TemplateService.getTemplateData(req.body);
                    // return res.json({'status':HttpCode.ok,'data':images});
                    return await res.json(categories);
                } catch (e) {
                    log.e(`${req.method}: ${req.url}`, e);
                    if (e.code === AppCode.s3_error) {
                        return res.status(HttpCode.bad_request).send(e);
                    }
                    return res.status(HttpCode.internal_server_error).send(e);
                }
            });

        router.post('/update', async (req, res) => {
            try {
                const templateData = 
                {
                    id : req.body.id,
                    marketId:req.body.marketId,
                    templateName : req.body.templateName,
                    language: req.body.language,
                    status : req.body.status
                };
                    await this.TemplateService.updateTemplateData(templateData);
                    return res.sendStatus(HttpCode.ok);
                } 
            catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                return res.sendStatus(HttpCode.internal_server_error);
                    }
            });

            
// //------------------------using get---------------------        


//             router.get('/delete/:galleryId', async (req, res) => {
//                 try {
//                     // console.log(req.params.galleryId);
//                     // return false;
//                     await this.GalleryService.deleteGalleryData(req.params.galleryId);
//                     return res.sendStatus(HttpCode.ok);
//                 } catch (e) {
//                     log.e(`${req.method}: ${req.url}`, e);
//                     return res.sendStatus(HttpCode.internal_server_error);
//                 }
//             });        


// //------------------using post--------------


                router.post('/delete',uploadFile, async (req, res) => {
                    try {
                        const templateData = {
                            id : req.body.id,
                            deletedAt : 'utc_timestamp()',
                        } ;
                        // console.log(templateData);
                        // return false;
                        await this.TemplateService.deleteTemplateData(templateData);
                        return res.sendStatus(HttpCode.ok);
                    } catch (e) {
                        log.e(`${req.method}: ${req.url}`, e);
                        return res.sendStatus(HttpCode.internal_server_error);
                    }
                });        

    }

}