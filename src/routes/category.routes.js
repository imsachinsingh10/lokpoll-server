import express from 'express';
import {validateAuthToken} from "../middleware/auth.middleware";
import {CategoryService} from "../service/category.service";
import AppOverrides from "../service/common/app.overrides";
import {log} from "../service/common/logger.service";
import {AppCode} from "../enum/app-code";
import {HttpCode} from "../enum/http-code";
import {MinIOService, uploadFile} from "../service/common/minio.service";


const router = express();
export class CategoryRoutes {
    constructor(app) {
        new AppOverrides(router);
        app.use('/category', router);

        this.CategoryService = new CategoryService();
        this.minioService = new MinIOService();
        this.initRoutes();
    }

    initRoutes() {
        router.use(validateAuthToken);

        router.post('/add', async (req, res) => {
           
            try {
                let categoryData = {
                    categoryName: req.body.categoryName,
                    language: req.body.language,
                    status: req.body.status,
                    marketId: req.body.marketId,
                
                }; 
                await this.CategoryService.addCategoryData(categoryData);
                return res.sendStatus(HttpCode.ok);
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.s3_error) {
                    return res.status(HttpCode.bad_request).send(e);
                }
                return res.status(HttpCode.internal_server_error).send(e);
            }
        });


        router.post('/list', async (req, res) => {
                try {
                    

                    let categories = await this.CategoryService.getCategoryData(req.body);
                    // return res.json({'status':HttpCode.ok,'data':images});
                    return await res.json(categories);
                } catch (e) {
                    log.e(`${req.method}: ${req.url}`, e);
                    if (e.code === AppCode.s3_error) {
                        return res.status(HttpCode.bad_request).send(e);
                    }
                    return res.status(HttpCode.internal_server_error).send(e);
                }
            });

        router.post('/update', async (req, res) => {
            try {
                const categoryData = 
                {
                    id : req.body.id,
                    marketId:req.body.marketId,
                    categoryName : req.body.categoryName,
                    language: req.body.language,
                    status : req.body.status
                };
                    await this.CategoryService.updateCategoryData(categoryData);
                    return res.sendStatus(HttpCode.ok);
                } 
            catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                return res.sendStatus(HttpCode.internal_server_error);
                    }
            });

            
// //------------------------using get---------------------        


            // router.get('/delete/:categoryId', async (req, res) => {
            //     try {
            //         // console.log(req.params.galleryId);
            //         // return false;
            //         deletedAt: 'utc_timestamp()';
            //         await this.CategoryService.deleteCategoryData(req.params.galleryId);
            //         return res.sendStatus(HttpCode.ok);
            //     } catch (e) {
            //         log.e(`${req.method}: ${req.url}`, e);
            //         return res.sendStatus(HttpCode.internal_server_error);
            //     }
            // });        


// //------------------using post--------------


            router.post('/delete',uploadFile, async (req, res) => {
                try {
                    const categoryData = {
                        id : req.body.id,
                        deletedAt : 'utc_timestamp()',
                    } ;
                    // console.log(categoryData);
                    // return false;
                    await this.CategoryService.deleteCategoryData(categoryData);
                    return res.sendStatus(HttpCode.ok);
                } catch (e) {
                    log.e(`${req.method}: ${req.url}`, e);
                    return res.sendStatus(HttpCode.internal_server_error);
                }
            });        

    }

}