import {QueryBuilderService} from "./sql/querybuilder.service";
import {SqlService} from "./sql/sql.service";
import {table} from "../enum/table";
import * as _ from 'lodash';

export class FormBuilderService {
    constructor() {
    }


    async createForm(frmBldData) {
        const query = QueryBuilderService.getInsertQuery(table.tbl_form, frmBldData);
        return SqlService.executeQuery(query);
    }

    async createInputFields(frmBldData) {
        const query = QueryBuilderService.getInsertQuery(table.tbl_form_input_fields, frmBldData);
        return SqlService.executeQuery(query);
    }
	

	async updateForm(frmBldData) {
        const condition = `where id = ${frmBldData.id} and deleted_at is null`;
        const query = QueryBuilderService.getUpdateQuery(table.tbl_form, frmBldData, condition);
        return SqlService.executeQuery(query);
    }


	async updateInputFields(frmBldData) {
        const condition = `where id = ${frmBldData.id} and form_id = ${frmBldData.form_id} and deleted_at is null`;
        const query = QueryBuilderService.getUpdateQuery(table.tbl_form_input_fields, frmBldData, condition);
        return SqlService.executeQuery(query);
    }


    async getForm(frmGetData) {
		 let slug = frmGetData.slug;
		 let id = frmGetData.id;
		 let query;
		 
		 /*
		 if (typeof slug !== 'undefined') {
		  query =`select a.id as form_id, a.name, a.slug, a.language, a.created_at 
		          from tbl_form a where a.slug = '${slug}' and a.deleted_at is null;`
		 }else{
			 query =`select a.id as form_id, a.name, a.slug, a.language, a.created_at 
		          from tbl_form a where a.id = '${id}' and a.deleted_at is null;`
		 }*/
		 
		  if (slug){
		  query =`select a.id as form_id, a.name, a.slug, a.language, a.created_at 
		          from tbl_form a where a.slug = '${slug}' and a.deleted_at is null;`;
		 }else if (id){
		  query =`select a.id as form_id, a.name, a.slug, a.language, a.created_at 
		            from tbl_form a where a.id = '${id}' and a.deleted_at is null;`; 
		 }
		 else{
			 query =`select a.id as form_id, a.name, a.slug, a.language, a.created_at 
		            from tbl_form a where a.deleted_at is null;`;
		 }
		 
		return SqlService.executeQuery(query);
    }
	
	async getFormInput(frmGetData) {
		 let form_id = frmGetData.form_id;
		 console.log(form_id);
		 
        let query =`select b.id as form_input_id, 
					b.label, b.input_type as type, 
					b.custom_value as customValueSet, 
					b.value_set as valueSet,
					b.default_value as defaultValue,
					b.default_value, 
					b.place_holder as placeholder, 
					b.require as requiredValue,
					b.show_input as showInput,
					b.show_label as showLabel
			from tbl_form_input_fields b where b.form_id = '${form_id}' and b.deleted_at is null;`;
		return SqlService.executeQuery(query);
    }
    
	async deleteForm(frmBldData) {
        const condition = `where id = ${frmBldData.id}`;
        const query = QueryBuilderService.getUpdateQuery(table.tbl_form, frmBldData, condition);
        return SqlService.executeQuery(query);
    }
	
	async deleteFormInput(frmBldData) {
        let query;
		const FieldValueData = await this.getFormInputById(frmBldData);
		if (FieldValueData[0]){
		    const condition = `where id = ${frmBldData.id} and form_id = ${frmBldData.form_id}`;
		    query = QueryBuilderService.getUpdateQuery(table.tbl_form_input_fields, frmBldData, condition);
		}else{
			query = `delete 
			from tbl_form_input_fields where id = ${frmBldData.id} and form_id = ${frmBldData.form_id};`;
		}
        return SqlService.executeQuery(query);
    }
	
	async getFormInputById(frmBldData) {
        const query = `select b.input_value 					
			from tbl_form_input_values b where b.form_input_id = ${frmBldData.id} and b.form_id = ${frmBldData.form_id};`;
        return SqlService.executeQuery(query);
    }
	
	
	async getFormById(frmGetData) {
		 let id = frmGetData.form_id;
		 let query =`select a.id as form_id, a.uid, a.name, a.slug, a.language  
		            from tbl_form a where a.id = '${id}' and a.deleted_at is null;`;
		 return SqlService.executeQuery(query);
    }
	
	async getAllFormsList(frmGetData) {
		 let limit = frmGetData.limit;
		 let page = frmGetData.page;
		 
		 if (! limit)
		 {limit=10;
		 }else if(limit==0)
		 {limit=10;
		 }
		 
         if (! page)
		 { page=1;
		 }else if (page==0)
		 {page=1;
		 }
		 
		 let offset = limit * (page-1);
		 
		 let query =`select a.id, a.name as formName, a.slug, a.language, a.created_at as createdAt 
		          from tbl_form a where a.deleted_at is null order by a.created_at desc LIMIT ${limit} offset ${offset};`;
		 return SqlService.executeQuery(query);
		 	 
    }
	
	async convertToSlug(textstr)
    {
	  
      var slug = textstr
	          .toLowerCase()
			  .replace(/ /g,'-')
			  .replace(/\s+/g, '-')
			  .replace(/[^\w\-]+/g, '')
			  .replace(/\-\-+/g, '-')
			  .replace(/^-+/, '')
			  .replace(/-+$/, '');
      
	  const slugval= await this.VerifySlug(slug);
	  if ( slugval.MaxSlug > 0 )
	  { 
        //console.log(slugval.MaxSlug);
        slug = slug + slugval.MaxSlug.toString();
		//console.log(slug);
		}
      return slug ;
    }
    
	
	
	async VerifySlug(slug) {
        let pval = slug.concat('%');
        const query = `select count(*) as MaxSlug
	    				from ${table.tbl_form} 
	    				where slug like '${pval}';`;
                        //console.log(query);
        return SqlService.getSingle(query);
    }
	
	
	
	async CheckSlugForCreateInputFields(slug){
	  const slugval= await this.VerifySlug(slug);
	  if ( slugval.MaxSlug > 0 )
	  { 
          return true;
       }
      return false ;
	}
	//''''''''''030321
	
	async GetCustomValueData(formDataInput, formData){
		/*'''''''''''''''''''''''''''''''''''''''''''''''
				  1. getCategoryForProduct
				  2. getCategoryForService
				  3. getCategoryForProperties
				'''''''''''''''''''''''''''''''''''''''''''''''''*/
				let customCategorySet = {
					                 getCategoryForProduct: "1",
									 getCategoryForService: "2",
									 getCategoryForProperties: "3"
				                     };
									 
				let customTemplateSet = {
					                 getTemplateForProduct: "1",
									 getTemplateForService: "2",
									 getTemplateForProperties: "3"
				                     };
									 
                let customAttributeSet = {
					                    getAttributes: "1"
				                     };
									 
									 
				for (let inputfld2 of formDataInput){
					if(inputfld2.customValueSet!="" && inputfld2.customValueSet!=null){
						 for (var key in customCategorySet) {
							  if(key == inputfld2.customValueSet) {
								  let bodyCategory={
										status: "active",
										marketId: customCategorySet[key],
										language: formData[0].language
									};
									let formDataCustomValueSet = await this.getCategoryData(bodyCategory);
									inputfld2.customData = formDataCustomValueSet;
								}
							   
							   
						   }
						   
						   for (var key in customTemplateSet) {
							  if(key == inputfld2.customValueSet) {
								  let bodyTemplate={
										status: "active",
										marketId: customTemplateSet[key],
										language: formData[0].language
									};
									let formDataCustomValueSet = await this.getTemplateData(bodyTemplate);
									//inputfld2.customValueData = formDataCustomValueSet;
									inputfld2.customData = formDataCustomValueSet;
								}
							   
							   
						   }
						   
						   for (var key in customAttributeSet) {
							  if(key == inputfld2.customValueSet) {
								  console.log(key);
									let bodyCategory={
										//categoryId: customAttributeSet[key]
									};
									let formDataCustomValueSet = await this.getAttributeData(bodyCategory);
									inputfld2.customData = formDataCustomValueSet;
								}
							   
							   
						   }
						
						
					}else{
						inputfld2.customValueSet = '';
						inputfld2.customData = [];
					}
				}
				
				return "Ok";
				//'''''''''''''''''''''''''''''''''''''''''''''''
	}
	
	async getCategoryData(body) {
			//let columns = 'g.id, g.categoryName, g.language as language_id, g.status, g.marketId,l.name as language, m.name as market';
			let columns = 'g.marketId, g.id, g.categoryName ';
			// let c1 = `and ((g.status='active') or (g.status='inactive'))`;
			let c1 = " ";
			// let c2 = `and ((g.marketId='1') or (g.marketId='2') or (g.marketId='3'))`;
			let c2 = " ";
			// let c3 = `and ((g.language='or') or (g.language='hi') or (g.language='en') or (g.language='ta'))`;
			let c3 = " ";
			let c4 = `and g.deletedAt is NULL`;
			if (body.status) {
				c1 = `and g.status = '${body.status}'`;
			}
			if (body.marketId) {
				c2 = `and g.marketId = '${body.marketId}'`;
			}
			if (body.language) {
				c3 = `and l.name = '${body.language.toLowerCase()}'`;
			}
			

			const query = `select ${columns}
							from ${table.category} g
							inner join ${table.language} l 
							on g.language = l.id
							inner join ${table.market} m
							on g.marketId = m.id
							where g.id > 0
							${c1} ${c2} ${c3} ${c4}
							order by g.id desc`;
							// console.log(query);
							// return false;
			return SqlService.executeQuery(query);
    }
	
	
	async getTemplateData(body) {
			//let columns = 'g.id, g.templateName, g.language as language_id, g.status, g.marketId,l.name as language, m.name as market';
			let columns = 'g.marketId, g.id, g.templateName ';
			// let c1 = `and ((g.status='active') or (g.status='inactive'))`;
			let c1 = " ";
			// let c2 = `and ((g.marketId='1') or (g.marketId='2') or (g.marketId='3'))`;
			let c2 = " ";
			// let c3 = `and ((g.language='or') or (g.language='hi') or (g.language='en') or (g.language='ta'))`;
			let c3 = " ";
			let c4 = `and g.deletedAt is NULL`;
			if (body.status) {
				c1 = `and g.status = '${body.status}'`;
			}
			if (body.marketId) {
				c2 = `and g.marketId = '${body.marketId}'`;
			}
			if (body.language) {
				c3 = `and l.name = '${body.language.toLowerCase()}'`;
			}
			

			const query = `select ${columns}
							from ${table.template} g
							inner join ${table.language} l 
							on g.language = l.id
							inner join ${table.market} m
							on g.marketId = m.id
							where g.id > 0
							${c1} ${c2} ${c3} ${c4}
							order by g.id desc`;
							// console.log(query);
							// return false;
			return SqlService.executeQuery(query);
	}
	
	async getAttributeData(body) {
        let columns = 'g.id, g.categoryId, c.categoryName as category, g.attributeName';
        let c1 = `and g.deletedAt is NULL`;
        const query = `select ${columns}
	    				from ${table.attribute} g
                        inner join ${table.category} c 
                        on g.categoryId = c.id
	    				where g.id > 0
	    				${c1} 
                        order by g.id asc`;
                        // console.log(query);
                        // return false;
        return SqlService.executeQuery(query);
    }
	//''''''''''''''''
	
	async getFormInputData(frmBldData) {
        const condition = `where form_input_id = ${frmBldData.form_input_id} and form_id = ${frmBldData.form_id}`;
		let query = `select input_value as value 
				from tbl_form_input_values ${condition} order by id asc ;`;

		return SqlService.executeQuery(query);
    }

}
