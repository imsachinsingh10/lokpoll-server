import {QueryBuilderService} from "./sql/querybuilder.service";
import {SqlService} from "./sql/sql.service";
import {table} from "../enum/table";
import _ from 'lodash';

export class AttributeService {
    
    
    async addAttributeData(attributeData) {
        const query = QueryBuilderService.getInsertQuery(table.attribute, attributeData);
        return SqlService.executeQuery(query);
    }

    async getAttributeData(body) {
        let columns = 'g.*, c.categoryName as category';
        let c1 = `and g.deletedAt is NULL`;
        const query = `select ${columns}
	    				from ${table.attribute} g
                        inner join ${table.category} c 
                        on g.categoryId = c.id
	    				where g.id > 0
	    				${c1} 
                        order by g.id asc`;
                        // console.log(query);
                        // return false;
        return SqlService.executeQuery(query);
    }

    async getAttributeValueData(attributeId) {
        let columns = 'av.id, av.attributeValueName';
        let c1 = `and av.attributeId = ${attributeId}`;
        const query = `select ${columns}
	    				from ${table.attribute_Value} av
	    				where av.id > 0
	    				${c1} 
                        order by av.id asc`;
                        // console.log(query);
                        // return false;
        return SqlService.executeQuery(query);
    }



    async updateAttributeData(attributeData) {
        const condition = `where id = ${attributeData.id}`;
        const query = QueryBuilderService.getUpdateQuery(table.attribute, attributeData, condition);
        return SqlService.executeQuery(query);
    }

// //------------using get--------------------

    // async deleteAttributeData(attributeId) {

    //     const query = `update ${table.attribute} set deletedAt utc_timestamp() where id = ${galleryId};`;
    //     // console.log(query);
    //     // return false;
    //     return SqlService.executeQuery(query);
    // }

// // ---------------------using post-------------- 


    async deleteAttributeData(attributeData) {
        const query = `update ${table.attribute} set deletedAt = ${attributeData.deletedAt} where id = ${attributeData.id};`;
 
        return SqlService.executeQuery(query);
    }
 }