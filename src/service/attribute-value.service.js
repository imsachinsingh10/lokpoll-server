import {QueryBuilderService} from "./sql/querybuilder.service";
import {SqlService} from "./sql/sql.service";
import {table} from "../enum/table";
import _ from 'lodash';

export class AttributeValueService {
    
    
    async addAttributeValueData(attributeValueData) {
        const query = QueryBuilderService.getInsertQuery(table.attribute_Value, attributeValueData);
        return SqlService.executeQuery(query);
    }

    
 }