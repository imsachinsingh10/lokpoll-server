import {QueryBuilderService} from "./sql/querybuilder.service";
import {SqlService} from "./sql/sql.service";
import {table} from "../enum/table";
import _ from 'lodash';

export class TemplateService {
    
    
    async addTemplateData(templateData) {
        const query = QueryBuilderService.getInsertQuery(table.template, templateData);
        return SqlService.executeQuery(query);
    }

    async getTemplateData(body) {
        let columns = 'g.id, g.templateName, g.language as language_id, g.status, g.marketId,l.name as language, m.name as market';
        // let c1 = `and ((g.status='active') or (g.status='inactive'))`;
        let c1 = " ";
        // let c2 = `and ((g.marketId='1') or (g.marketId='2') or (g.marketId='3'))`;
        let c2 = " ";
        // let c3 = `and ((g.language='or') or (g.language='hi') or (g.language='en') or (g.language='ta'))`;
        let c3 = " ";
        let c4 = `and g.deletedAt is NULL`;
        if (body.status) {
            c1 = `and g.status = '${body.status}'`;
        }
        if (body.marketId) {
            c2 = `and g.marketId = '${body.marketId}'`;
        }
        if (body.language) {
            c3 = `and g.language = '${body.language}'`;
        }
        

        const query = `select ${columns}
	    				from ${table.template} g
                        inner join ${table.language} l 
                        on g.language = l.id
                        inner join ${table.market} m
                        on g.marketId = m.id
	    				where g.id > 0
	    				${c1} ${c2} ${c3} ${c4}
                        order by g.id desc`;
                        // console.log(query);
                        // return false;
        return SqlService.executeQuery(query);
    }

    async updateTemplateData(templateData) {
        const condition = `where id = ${templateData.id}`;
        const query = QueryBuilderService.getUpdateQuery(table.template, templateData, condition);
        return SqlService.executeQuery(query);
    }

// //------------using get--------------------

    // async deleteTemplateData(templateId) {

    //     const query = `update ${table.template} set deletedAt utc_timestamp() where id = ${galleryId};`;
    //     // console.log(query);
    //     // return false;
    //     return SqlService.executeQuery(query);
    // }

// // ---------------------using post-------------- 


    async deleteTemplateData(templateData) {
        const query = `update ${table.template} set deletedAt = ${templateData.deletedAt} where id = ${templateData.id};`;
 
        return SqlService.executeQuery(query);
    }
 }