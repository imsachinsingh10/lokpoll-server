import {QueryBuilderService} from "./sql/querybuilder.service";
import {SqlService} from "./sql/sql.service";
import {table} from "../enum/table";
import _ from 'lodash';

export class NotificationService {


    async addNotificationData(notificationData) {
        const query = QueryBuilderService.getInsertQuery(table.notificationAPI, notificationData);
        return SqlService.executeQuery(query);
    }

    async getNotificationData(body) {
        let columns = 'n.*';
        let c1 = `and (n.status='1') or (n.status='0')`;
        if (body.status) {
            c1 = `and n.status = '${body.status}'`;
        }

        const query = `select ${columns}
	    				from ${table.notificationAPI} n
	    				where id > 0
	    				${c1}
                        order by n.id desc`;
                        // console.log(query);
                        // return false;
        return SqlService.executeQuery(query);
    }

    async updateNotificationData(notificationData) {
        const condition = `where id = ${notificationData.id}`;
        const query = QueryBuilderService.getUpdateQuery(table.notificationAPI, notificationData, condition);
        return SqlService.executeQuery(query);
    }

    //------------using get--------------------

    async deleteNotificationData(notificationId) {
        const query = `delete from ${table.notificationAPI} where id = ${notificationId};`;
        // console.log(query);
        // return false;
        return SqlService.executeQuery(query);
    }

}